// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Fish"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_Albedo("Albedo", 2D) = "white" {}
		_AlbedoColor("AlbedoColor", Color) = (1,1,1,1)
		_Emission("Emission", 2D) = "white" {}
		_EmissionColor("EmissionColor", Color) = (1,1,1,1)
		_EmissionStrength("EmissionStrength", Range( 0 , 10)) = 0.4
		_FishMovementTexture("FishMovementTexture", 2D) = "white" {}
		_SwimStrength("SwimStrength", Vector) = (1,0,0,0)
		_Speed("Speed", Range( 0 , 1)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float2 texcoord_0;
			float2 texcoord_1;
		};

		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _AlbedoColor;
		uniform sampler2D _Emission;
		uniform float4 _Emission_ST;
		uniform float4 _EmissionColor;
		uniform float _EmissionStrength;
		uniform sampler2D _FishMovementTexture;
		uniform float _Speed;
		uniform float3 _SwimStrength;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.texcoord_0.xy = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			o.texcoord_1.xy = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float2 temp_cast_0 = (o.texcoord_1.x).xx;
			float4 temp_output_19_0 = ( float4( 0,0,0,0 ) + tex2Dlod( _FishMovementTexture, float4( (abs( temp_cast_0+( _Time.y * _Speed ) * float2(1,1 ))), 0.0 , 0.0 ) ) );
			v.vertex.xyz += ( temp_output_19_0 * float4( _SwimStrength , 0.0 ) ).xyz;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			o.Albedo = ( tex2D( _Albedo, uv_Albedo ) * _AlbedoColor ).xyz;
			float2 uv_Emission = i.uv_texcoord * _Emission_ST.xy + _Emission_ST.zw;
			float luminance28 = Luminance( tex2D( _Emission, uv_Emission ).xyz );
			float2 temp_cast_3 = (i.texcoord_0.x).xx;
			float4 temp_output_19_0 = ( float4( 0,0,0,0 ) + tex2D( _FishMovementTexture, (abs( temp_cast_3+( _Time.y * _Speed ) * float2(1,1 ))) ) );
			o.Emission = ( luminance28 * _EmissionColor * _EmissionStrength * temp_output_19_0 ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13101
7;216;2175;1196;723.1119;1083.433;1.513023;True;False
Node;AmplifyShaderEditor.RangedFloatNode;29;-43.76445,388.7386;Float;False;Property;_Speed;Speed;7;0;1;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleTimeNode;8;73.4062,270.8924;Float;False;1;0;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;270.9443,346.3738;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;6;16.40625,153.8925;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.PannerNode;4;303.4062,231.8925;Float;False;1;1;2;0;FLOAT2;0,0;False;1;FLOAT;0.0;False;1;FLOAT2
Node;AmplifyShaderEditor.SamplerNode;5;505.406,149.8925;Float;True;Property;_FishMovementTexture;FishMovementTexture;5;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;20;575.4003,-240.6815;Float;True;Property;_Emission;Emission;2;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;7;664.666,-430.2374;Float;False;Property;_AlbedoColor;AlbedoColor;1;0;1,1,1,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.LuminanceHlpNode;28;936.4003,-148.6815;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT
Node;AmplifyShaderEditor.SamplerNode;21;586.4003,-609.6815;Float;True;Property;_Albedo;Albedo;0;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;19;832.5769,133.9185;Float;True;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0.0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.ColorNode;22;673.4003,-42.68152;Float;False;Property;_EmissionColor;EmissionColor;3;0;1,1,1,1;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.Vector3Node;9;855.688,370.2143;Float;False;Property;_SwimStrength;SwimStrength;6;0;1,0,0;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;27;357.3845,19.91755;Float;False;Property;_EmissionStrength;EmissionStrength;4;0;0.4;0;10;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;910.4003,-528.6815;Float;True;2;2;0;FLOAT4;0.0;False;1;COLOR;0.0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;1092.4,5.318481;Float;False;4;4;0;FLOAT;0.0,0,0,0;False;1;COLOR;0;False;2;FLOAT;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;1092.76,199.6829;Float;True;2;2;0;FLOAT4;0,0,0;False;1;FLOAT3;0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1356,-5;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Fish;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;30;0;8;0
WireConnection;30;1;29;0
WireConnection;4;0;6;1
WireConnection;4;1;30;0
WireConnection;5;1;4;0
WireConnection;28;0;20;0
WireConnection;19;1;5;0
WireConnection;23;0;21;0
WireConnection;23;1;7;0
WireConnection;24;0;28;0
WireConnection;24;1;22;0
WireConnection;24;2;27;0
WireConnection;24;3;19;0
WireConnection;10;0;19;0
WireConnection;10;1;9;0
WireConnection;0;0;23;0
WireConnection;0;2;24;0
WireConnection;0;11;10;0
ASEEND*/
//CHKSM=DF9EA705FDAD12DC4777DEFA6339C81318DE1AB2