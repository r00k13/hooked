using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

//Interfaces with Fungus, triggering the appropriate blocks in the flowchart and pausing the game while they play. Also sets and gets variables in the flowchart for use by other scripts.

public class FungusManager : MonoBehaviour
{
    public Flowchart flowchart;
    public Image dialogueBox;
    public ButtonPrompts buttonPrompts;
    private float chatEndTime = 0f;

    private void Start()
    {
        flowchart.ExecuteBlock("FadeIn");                                   //Fade scene in from black at start
    }

    public void StartChat(string person, int chatID)                        //Play a specific dialogue
    {
        if(Time.timeSinceLevelLoad > chatEndTime + 3f)                      //Check if at least 3 seconds have passed since last conversation, to keep the player from restarting if they button mashed through it
        {
            if (flowchart.FindBlock(person + chatID) != null)
            {
                GameManager.paused = true;
                buttonPrompts.Hide();
                flowchart.ExecuteBlock(person + chatID);
            }
            else
            {
                Debug.Log("Block " + person + chatID + " does not exist");
            }
        }  
    }

    public void Resume()                                                    //Called from the flowchart whenever a dialogue ends. Unpauses the game
    {
        chatEndTime = Time.timeSinceLevelLoad;                                       
        GameManager.paused = false;
    }

    public void SetFlowchartBool(string key, bool value)                    //Set a bool in the flowchart
    {
        flowchart.SetBooleanVariable(key, value);
    }

    public bool GetFlowchartBool(string key)                                //Get a bool from the flowchart
    {
        return flowchart.GetBooleanVariable(key);
    }

    public void SetDialogueBox(Sprite sprite)                               //Set the sprite used on the dialogue box
    {
        dialogueBox.sprite = sprite;
    }
}
