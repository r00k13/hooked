﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSpawner : MonoBehaviour
{
    public GameObject fishPrefab;
    public float spawnRate = 2f;
    private float nextSpawn = 0f;
    private Transform player;
    public int fishCount = 1;                                                                   //Current number of fish in the scene
    public int maxFish = 30;                                                                    //Maximum number of fish in the scene

	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	void Update ()
    {
		if(Time.timeSinceLevelLoad > nextSpawn && fishCount < maxFish)
        {
            nextSpawn = Time.timeSinceLevelLoad + spawnRate;

            float x = Random.Range(player.position.x - 50, player.position.x + 50);
            float z = Random.Range(player.position.z - 50, player.position.z + 50);

            Instantiate(fishPrefab, new Vector3(x, -1, z), fishPrefab.transform.rotation);

            fishCount++;
        }
	}
}
