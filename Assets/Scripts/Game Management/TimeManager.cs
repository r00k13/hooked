using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Contains a float which represents the current in-game time. As the value of this float increases, triggers events, rotates the sun and moon, 
//changes the colour and intensity of the sun�s light, and moves the hands on the UI clock. 

public class TimeManager : MonoBehaviour
{
    public static float clock = 11f;                                                    //Current time
    public float timeTick = 0.05f;                                                      //Rate at which time passes

    public GameObject sun, moon;                                                        //Directional lights that rotate over time
	private Light sunLight, moonLight;
	
	public Color sunYellow;                                                             //Colours used for changing at sunrise & sunset
    public Color sunRed;
    public Color sunPurple;

    public GameObject clockUI;                                                          //Clock on the UI
    public RectTransform bigHand, smallHand;

    public delegate void TimeAction();
    public static event TimeAction Morning, Afternoon, Evening, Nightfall;              //Events triggered at specific times of day
    private int dayStage = 1;                                                           //Tracks which event was last triggered to prevent multiple triggers
	
	void Start()
	{
		sunLight = sun.GetComponent<Light>();
        moonLight = moon.GetComponent<Light>();
        clock = 12f;
	}
	
    void Update ()
    {
        if(Input.GetKeyDown(KeyCode.U))                                                 //Toggle UI on & off, used when recording footage for trailers
        {
            clockUI.transform.parent.gameObject.SetActive(!clockUI.activeInHierarchy);
        }

        if (GameManager.paused)
        {
            clockUI.SetActive(false);                                                   //Hide the clock when the game is paused
        }
        else
        {
            clock += Time.deltaTime * timeTick;                                         //Increase current time

            clockUI.SetActive(true);

            float minutes = Mathf.FloorToInt((clock - Mathf.FloorToInt(clock)) * 60);   //Calculate minutes to show on clock 
            bigHand.transform.eulerAngles = new Vector3(0, 0, 360f - minutes * 6f);     //Set rotation of hands on UI clock 
            smallHand.transform.eulerAngles = new Vector3(0, 0, 360f - clock * 30f);
        }

        if(Input.GetKey(KeyCode.T))                                                     //Speed up time while the T key is held down
        {
            timeTick = 1f;
        }
        else
        {
            timeTick = 0.05f;
        }

        sun.transform.eulerAngles = new Vector3(((clock / 33) * 360) + 320, -90, 0);       //Rotate sun & moon based on current time
        moon.transform.eulerAngles = new Vector3(((clock / 24) * 360) + 90, -90, 0);

        if (clock >= 24 /*&& !GameManager.paused*/)                                       //If it's after midnight, end day
        {
            clock = 0f;
        }
        else if (clock >= 21)                                                           //If it's after 21:00, turn off the sun
        {
            //sun.SetActive(false);
            if (moonLight.intensity < 0.1f)
            {
                moonLight.intensity += Time.deltaTime * 0.01f;
            }
        }
        //else if (clock >= 19f && dayStage == 3)                                         //If it's after 19:00 & Nightfall event hasn't been triggered, trigger it & turn on the moon
        //{
        //    if(Nightfall != null)
        //    {
        //        Nightfall();
        //    }
        //    dayStage++;
        //    //moon.SetActive(true);
        //}

        else if (clock >= 19f)                                                        //If it's after 19:30, reduce sun intensity
        {
            sunLight.color = Color.Lerp(sunLight.color, sunPurple, Time.deltaTime / 5); //Slowly change sun colour to purple
            if (sunLight.intensity > 0)
            {
                sunLight.intensity -= Time.deltaTime * 0.08f;
            }
            else if (moonLight.intensity < 0.1f)
            {
                moonLight.intensity += Time.deltaTime * 0.01f;
            }
        }
        else if (clock >= 18f)                                                          //If it's after 18:00, change sun colour to red
        {
            sunLight.color = Color.Lerp(sunLight.color, sunRed, Time.deltaTime / 5);    //Slowly change sun colour to red
        }
        //else if (clock >= 17f && dayStage == 2)                                       //If it's after 17:00 & Evening event hasn't been triggered, trigger it
        //{
        //    if(Evening != null)
        //    {
        //        Evening();
        //    }
        //    dayStage++;
        //}
        //else if (clock >= 12f && dayStage == 1)                                         //If it's after 12:00 & Afternoon event hasn't been triggered, trigger it
        //{
        //    if (Afternoon != null)
        //    {
        //        Afternoon();
        //    }
        //    dayStage++;
        //}
        else if (clock >= 7f)                                           					//If it's after 7:00
        {
            //if(dayStage == 0)															//If Morning event hasn't been triggered, trigger it
            //{
            //	if (Morning != null)
            //	{
            //		Morning();
            //	}
            //	dayStage++;
            //}

            sunLight.color = Color.Lerp(sunLight.color, sunYellow, Time.deltaTime / 5); //Slowly change sun colour to yellow
        }
        else if (clock >= 5f)                                                            //If it's before 7:00, reset daystage to 0 & reset sun & moon
        {
            //dayStage = 0;

            //sun.SetActive(true);
            //moon.SetActive(false);
            if (sunLight.intensity < 0.8f)
            {
                sunLight.intensity += Time.deltaTime * 0.05f;
            }
            sunLight.color = Color.Lerp(sunLight.color, sunRed, Time.deltaTime / 5);
        }
        else if (clock >= 4f)                                                            //If it's before 5:00, reset daystage to 0 & reset sun & moon
        {
            if (moonLight.intensity > 0)
            {
                moonLight.intensity -= Time.deltaTime * 0.01f;
            }
        }
    }
}
