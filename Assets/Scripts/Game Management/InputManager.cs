using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//Unifies all inputs into a set of static variables accessible by other scripts.

public class InputManager : MonoBehaviour
{
    public enum InputType {Mouse, XBOX, PS4, Steam, Unknown}                        //Enum for all possible input types
    public static InputType type = InputType.Mouse;

    public static float cameraX = 0;                                                //Static variables used by other scripts
    public static float triggerL = 0;
    public static float triggerR = 0;

    public static bool menu = false;
    public static bool submit = false;
    public static bool cancel = false;

    public static bool menuDown = false;
    public static bool submitDown = false;
    public static bool cancelDown = false;

    private float delayTime = 0f;                                                   //Delay timer, to avoid multiple button presses 

    private StandaloneInputModule inputModule;

    void Start()
    {
        inputModule = GetComponent<StandaloneInputModule>();

        if (Input.GetJoystickNames().Length > 0)                                    //If a controller is plugged in
        { 
            if (Input.GetJoystickNames()[0] == "Wireless Controller" || Input.GetJoystickNames()[0] == "Sony Computer Entertainment Wireless Controller" || Input.GetJoystickNames()[0] == "Sony Interactive Entertainment Wireless Controller")
            {
                SetInputMode(InputType.PS4);                                        //Set input type to PS4
                Debug.Log("PS4 controller detected");                               
            }
            else if (Input.GetJoystickNames()[0] == "Controller (XBOX 360 For Windows)" || Input.GetJoystickNames()[0] == "Controller (Xbox One For Windows)")
            {
                SetInputMode(InputType.XBOX);                                       //Set input type to XBOX
                Debug.Log("XBOX controller detected");
            }
            else
            {
                SetInputMode(InputType.Unknown);                                    //Set input type to unknown
                Debug.Log("Unknown controller detected: " + Input.GetJoystickNames()[0]);
            }
        }
        else
        {
            SetInputMode(InputType.Mouse);                                          //Set input type to mouse & keyboard
            Debug.Log("No controller detected");
        }
	}

    private void Update()
    {
        if (type == InputType.PS4)                                                   //If input type is PS4, get input from PS4 inputs
        {
            if (Input.GetButton("Menu PS4"))
            {
                menu = true;
            }
            else
            {
                menu = false;
            }

            if (Input.GetButton("Submit PS4"))
            {
                submit = true;
            }
            else
            {
                submit = false;
            }

            if (Input.GetButton("Cancel PS4"))
            {
                cancel = true;
            }
            else
            {
                cancel = false;
            }

            if (Input.GetButtonDown("Menu PS4"))
            {
                menuDown = true;
            }
            else
            {
                menuDown = false;
            }

            if (Input.GetButtonDown("Submit PS4"))
            {
                submitDown = true;
            }
            else
            {
                submitDown = false;
            }

            if (Input.GetButtonDown("Cancel PS4"))
            {
                cancelDown = true;
            }
            else
            {
                cancelDown = false;
            }

            cameraX = Input.GetAxis("Camera X PS4") * 2;

            triggerL = (Input.GetAxis("Trigger L PS4") + 1) / 2;                    //Get axes, modify so they're a value between 0 & 1 instead of -1 & 1
            triggerR = (Input.GetAxis("Trigger R PS4") + 1) / 2;
        }
        else                                                                        //Otherwise, get input from normal inputs (XBOX & mouse & keyboard work the same, hopefully others would too)
        {
            if (Input.GetButton("Menu"))
            {
                menu = true;
            }
            else
            {
                menu = false;
            }

            if (Input.GetButton("Submit"))
            {
                submit = true;
            }
            else
            {
                submit = false;
            }

            if (Input.GetButton("Cancel"))
            {
                cancel = true;
            }
            else
            {
                cancel = false;
            }

            if (Input.GetButtonDown("Menu"))
            {
                menuDown = true;
            }
            else
            {
                menuDown = false;
            }

            if (Input.GetButtonDown("Submit"))
            {
                submitDown = true;
            }
            else
            {
                submitDown = false;
            }

            if (Input.GetButtonDown("Cancel"))
            {
                cancelDown = true;
            }
            else
            {
                cancelDown = false;
            }

            if (type == InputType.XBOX || type == InputType.Unknown)                //If camera X is coming from an analogue stick, double it, as analogue stick values only range between -1 & 1
            {
                cameraX = Input.GetAxis("Camera X") * 2;
            }
            else
            {
                cameraX = Input.GetAxis("Camera X");
            }

            if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.OSXPlayer)     //If running on mac, need to use PS4 inputs instead, because fuck Apple
            {
                triggerL = (Input.GetAxis("Trigger L PS4") + 1) / 2;                    //Get axes, modify so they're a value between 0 & 1 instead of -1 & 1
                triggerR = (Input.GetAxis("Trigger R PS4") + 1) / 2;
            }
            else
            {
                triggerL = Input.GetAxis("Trigger L");                                 //Get axes, which are already a value between 0 & 1 instead of -1 & 1
                triggerR = Input.GetAxis("Trigger R");
            }
        } 
    }

    public void SetInputMode(InputType i)
    {
        switch (i)                                          
        {
            case InputType.Mouse:
                type = InputType.Mouse;                                             //Set input type to mouse & keyboard
                inputModule.submitButton = "Submit";
                break;
            case InputType.XBOX:
                type = InputType.XBOX;                                              //Set input type to XBOX
                inputModule.submitButton = "Submit";
                break;
            case InputType.PS4:
                type = InputType.PS4;                                               //Set input type to PS4
                inputModule.submitButton = "Submit PS4";
                break;
            case InputType.Steam:
                type = InputType.Steam;                                             //Set input type to steam controller
                inputModule.submitButton = "Submit";
                break;
            case InputType.Unknown:
                type = InputType.Unknown;                                           //Set input type to unknown
                inputModule.submitButton = "Submit";
                break;
        }
    }
}
