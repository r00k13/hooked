using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

//Tracks the player�s progress in the game and saves and loads the game using JSON files.

public class GameManager : MonoBehaviour
{
    public static bool paused = false;                                      //Static variable used to track when game is paused. Used instead of Time.timeScale, as that interferes with Fungus

    private SaveData saveData;                                              //SaveData object

    public GameObject saveIcon;

    void Start()
    {
        Load();                                                             //Attempt to load a game
    }

    private void Update()
    {
        if(InputManager.menu)
        {
            SceneManager.LoadScene(0);
        }
    }

    public void Save()
    {
        saveData = new SaveData();                                          //Create save data & assign variables

        saveIcon.SetActive(true);
        string jsonText = JsonUtility.ToJson(saveData);                     //Convert save data to JSON format
        File.WriteAllText(Application.persistentDataPath + "/" + "save.json", jsonText);        //Save JSON in file
    }

    private void Load()
    {
        string file = Application.persistentDataPath + "/" + "save.json";

        if (File.Exists(file))                                              //Attempt to load file    
        {
            string jsonFile = File.ReadAllText(file);                       //Read JSON from file

            saveData = new SaveData();                                      //Create save data & assign values from JSON to variables
            saveData = JsonUtility.FromJson<SaveData>(jsonFile);
        }
        else                                                                //If no save data was found, start game from beginning
        {
            Debug.Log("FILE NOT FOUND: " + file);
        }
    }
}

public struct SaveData
{
    public int data;
}
