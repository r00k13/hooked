﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoatManager : MonoBehaviour
{
    public int state = 0;
    private Boat boatScript;
    private GameObject fisher;

    public Text caughtText;

    public float fishCaught = 0;

    public GameObject bPFish, bPCancel, bPCatch;

    private void Start()
    {
        boatScript = GetComponent<Boat>();                                          //Retrieve script components
        fisher = GetComponentInChildren<Fisher>().gameObject;
        fishCaught = 0;

        SetDriving();
        HideCatch();                                                                //Hide catch prompt
    }

    void Update ()
    {
        caughtText.text = "Fish Caught: " + (int)fishCaught + "kg";                 //Set fish caught UI element
    }

    public void SetDriving()
    {
        boatScript.enabled = true;                                                  //Enable boat driving script
        state = 0;
        fisher.SetActive(false);
        bPFish.SetActive(true);
        bPCancel.SetActive(false);
    }

    public void SetFishing()
    {
        boatScript.enabled = false;                                                 //Disable boat driving script 
        state = 1;
        fisher.SetActive(true);
        bPCancel.SetActive(true);
        bPFish.SetActive(false);
    }

    public void ShowCatch()
    {
        bPCatch.SetActive(true);                                                    //Enable catch button prompt
    }

    public void HideCatch()
    {
        bPCatch.SetActive(false);                                                   //Disable catch button prompt
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Dock")                                         //If entering dock area, drop off all caught fish
        {
            Settlement settlement = other.gameObject.GetComponentInParent<Settlement>();

            if(settlement != null)
            {
                settlement.AddFish(fishCaught);
                fishCaught = 0;
            }
        }
    }
}
