using UnityEngine;
using System.Collections;

//Handles player input from the Input Manager, moves & animates the character, and calls other managers when the player interacts with something
//Also contains functions to move the player home, and make the player mesh look at a point.

public class PlayerManager : MonoBehaviour
{
    public float speed;                                                                                             //Movement speed
    public Animator animate;
    public GameObject mesh;                                                                                         //Player model, a child of the player
    private Vector3 meshRot;                                                                                        //Rotation of player model

    //private FungusManager fungusManager;
    public ButtonPrompts buttonPrompts;

    public Transform home;

    private void Start()
    {
        GameObject manager = GameObject.FindGameObjectWithTag("Manager");                                           //Retrieve managers
        //fungusManager = manager.GetComponent<FungusManager>();
    }

    void Update()
    {
        if (!GameManager.paused)
        {
            Vector3 dir = transform.forward * Input.GetAxis("Vertical") + transform.right * Input.GetAxis("Horizontal");    //Get input & create a direction vector from it
            dir = Vector3.ClampMagnitude(dir, 1);                                                                   //Clamp magnitute of direction vector to 1, to avoid character moving faster diagonally
            gameObject.GetComponent<CharacterController>().SimpleMove(dir * speed);                                 //Move character (time.deltatime not necessary with simplemove)
            animate.SetFloat("MovSpeed", dir.magnitude);                                                            //Set animation speed modifier to magnitude of direction vector, so animation is slower if analogue stick is not pushed fully

            if ((Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) && !GameManager.paused)        //If any movement is taking place & game is not paused
            {
                animate.SetBool("Moving", true);                                                                    //Tell the animator to use the moving animation
                mesh.transform.LookAt(mesh.transform.position + dir);                                               //Rotate the character model to face in the direction of movement
            }
            else
            {
                animate.SetBool("Moving", false);                                                                   //Tell the animator to use the idle animation
                mesh.transform.eulerAngles = meshRot;                                                               //Set mesh rotation to old rotation (to cancel out any camera movement)
            }
            meshRot = mesh.transform.eulerAngles;                                                                   //Record old rotation
        }
        else
        {
            animate.SetBool("Moving", false);
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Note" || collision.gameObject.tag == "Character" || collision.gameObject.tag == "Door" || collision.gameObject.tag == "Bed")   //Show button prompts when player enters tagged trigger zone
        {
            buttonPrompts.Show();
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (InputManager.submit && !GameManager.paused)                                                             //If player clicks while in trigger zone 
        {
            if (collision.gameObject.tag == "Character")
            {
                animate.SetBool("Moving", false);
                buttonPrompts.Hide();
                //fungusManager.StartChat(collision.gameObject.name);                                                 //Trigger a conversation using the name of the character
            }
            else if (collision.gameObject.tag == "Door")
            {
                buttonPrompts.Hide();
                Door door = collision.gameObject.GetComponent<Door>();

                if(door.locked)                                                                                     //If door is locked, trigger a random door dialogue
                {
                    //fungusManager.StartChat("Door", Random.Range(0, 4));
                }
                else                                                                                                //Otherwise, open the door
                {
                    door.Open();
                }
            }
            else if (collision.gameObject.tag == "Bed")
            {
                if(TimeManager.clock > 19f)                                                                         //If it's after 7pm, end the day
                {
                    //fungusManager.StartChat("NewDay", 0);
                }
                else                                                                                                //Otherwise, trigger a dialogue
                {
                    //fungusManager.StartChat("Bed", 0);
                }
            }
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if(collision.gameObject.tag == "Note" || collision.gameObject.tag == "Character" || collision.gameObject.tag == "Door" || collision.gameObject.tag == "Bed")    //Hide button prompts when player leaves tagged trigger zone
        {
            buttonPrompts.Hide();
        }
    }

    public void GoHome()                                                                                            //Return player to home position & move camera to match
    {
        transform.position = home.position;
        transform.rotation = home.rotation;

        Camera.main.transform.position = home.position + new Vector3(0, 5, -5);
        Camera.main.transform.rotation = home.rotation;
    }

    public void SetMeshLook(Vector3 target)                                                                         //Make mesh look at a specific point
    {
        mesh.transform.LookAt(target);
        meshRot = mesh.transform.eulerAngles;
    }
}
