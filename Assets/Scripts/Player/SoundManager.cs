using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

//Responsible for playing any sounds triggered by the player�s actions, as well as the Situational Ambient Sound system. 
//Alters the volume of the 2 music channels while the player is in a creepy zone, tracks how unsettling their current situation 
//is and plays appropriate sounds at random, and contains a function that can be called from other scripts to play a triggered sound.

public class SoundManager : MonoBehaviour
{
    public AudioMixer mixer;
    private float creepyVolume = 0f;                                //Volume modifier for music
    private bool creepy = false;                                    //Is the player in a creepy zone?

    private AudioSource sASound, triggeredSound;
    public AudioClip[] backgroundSounds;                            //Sounds for Situational Ambient Sound

    private float nextSoundTime = 3f;
    private int soundStage = 0;

    public static float musicMultiplier = 1f;                       //Volume multipliers for sound channels
    public static float sFXMultiplier = 1f;

	void Start ()
    {
        sASound = GetComponents<AudioSource>()[0];                  //Retrieve audio sources
        triggeredSound = GetComponents<AudioSource>()[1];
	}

    void OnEnable()
    {
        TimeManager.Morning += ResetSoundStage;                     //Add event listeners
        TimeManager.Afternoon += IncreaseSoundStage;
        TimeManager.Evening += IncreaseSoundStage;
        TimeManager.Nightfall += IncreaseSoundStage;
    }

    void OnDisable()
    {
        TimeManager.Morning -= ResetSoundStage;                     //Remove event listeners
        TimeManager.Afternoon -= IncreaseSoundStage;
        TimeManager.Evening -= IncreaseSoundStage;
        TimeManager.Nightfall -= IncreaseSoundStage;
    }

    void Update ()
    {
        if(creepy && creepyVolume < 1f)                             //If in a creepy zone, increase creepyVolume to a maximum of 1
        {
            creepyVolume += Time.deltaTime * 5f;
        }
        else if (!creepy && creepyVolume > 0)                       //If not in a creepy zone, decrease creepyVolume to a minimum of 0
        {
            creepyVolume -= Time.deltaTime * 5f;
        }

        mixer.SetFloat("musicMainVolume", -80f + 100f * musicMultiplier - (creepyVolume * 80f)); //Apply modifiers to audio channels. Add/subtract creepy to music channels, so one gets louder as the other gets quieter
        mixer.SetFloat("musicGlitchVolume", -80f + creepyVolume * 95f * musicMultiplier);

        mixer.SetFloat("ambientSFXVolume", -80f + 100f * sFXMultiplier);
        mixer.SetFloat("triggeredSFXVolume", -80f + 100f * sFXMultiplier);

        if (Time.timeSinceLevelLoad > nextSoundTime)
        {
            nextSoundTime = Time.timeSinceLevelLoad + Random.Range(6f, 12f);    //Pick next time to play a sound

            int soundNumber = soundStage + Mathf.FloorToInt(Vector3.Distance(transform.position, Vector3.zero) / 120f) + Random.Range(-3, 3); //Add percentage of distance from town, & add or subtract random modifier to SoundStage
            soundNumber = Mathf.Clamp(soundNumber, 0, backgroundSounds.Length - 1);     //Clamp value to size of array

            sASound.clip = backgroundSounds[soundNumber];           //Play the chosen sound
            sASound.Play();
        }
	}

    private void IncreaseSoundStage()                               //Increase soundStage, called by time of day events
    {
        soundStage++;
    }

    private void ResetSoundStage()                                  //Reset soundStage to 0, called once in the morning
    {
        soundStage = 0;
    }

    public void TriggerSound(AudioClip clip)                        //Play the clip that is passed in
    {
        triggeredSound.clip = clip;
        triggeredSound.Play();
    }

    void OnTriggerEnter(Collider collision)                         //If player enters a creepy zone, set creepy to true
    {
        if (collision.gameObject.tag == "Creepy")
        {
            creepy = true;
        }
    }

    void OnTriggerExit(Collider collision)                          //If player leaves a creepy zone, set creepy to false
    {
        if (collision.gameObject.tag == "Creepy")
        {
            creepy = false;
        }
    }
}
