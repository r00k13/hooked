﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fisher : MonoBehaviour
{
    private GameObject bobber;
    public GameObject bobberPrefab;
    public LineRenderer line;
    private BoatManager bm;
    private float castStrength = 0;

    public Image castStrengthBar;

    private void Start()
    {
        bm = GetComponentInParent<BoatManager>();
    }

    void OnEnable ()
    {
        Vector3 euler = new Vector3(0, Camera.main.transform.eulerAngles.y, 0);
        transform.eulerAngles = euler;
        castStrengthBar.gameObject.SetActive(true);

    }

    void Update ()
    {
        if (bobber != null)
        {
            line.SetPosition(0, line.transform.position);
            line.SetPosition(1, bobber.transform.position);

            transform.LookAt(new Vector3(bobber.transform.position.x, transform.position.y, bobber.transform.position.z));
        }
        else
        {
            line.SetPosition(0, line.transform.position);
            line.SetPosition(1, line.transform.position);

            castStrength = Mathf.PingPong(Time.time * 10, 15);
            castStrengthBar.fillAmount = castStrength / 15;

            transform.Rotate(new Vector3(0, InputManager.cameraX * Time.deltaTime * 80, 0));

            if (InputManager.submitDown)
            {
                bobber = Instantiate(bobberPrefab, transform.position + transform.forward * (5f + castStrength), transform.rotation);
                bobber.transform.position = new Vector3(bobber.transform.position.x, 0, bobber.transform.position.z);
                bobber.GetComponent<Bobber>().SetBobber(bm);

                castStrengthBar.gameObject.SetActive(false);
            }
        }

        if (InputManager.cancelDown)
        {
            if(bobber != null)
            {
                Destroy(bobber);
            }
            castStrengthBar.gameObject.SetActive(false);
            bm.SetDriving();
        }
    }
}
