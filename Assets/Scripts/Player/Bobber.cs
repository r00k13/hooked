﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bobber : MonoBehaviour
{
    private bool hooked = false;
    public GameObject splash, arrow;
    private float strength, weight;
    private BoatManager bm;

    private int direction = 0;                                          //Int between 0 & 3 to record current direction

    private float nextChange = 0;                                       //Time of next direction change

	void Update ()
    {
        transform.eulerAngles = new Vector3(0, Camera.main.transform.eulerAngles.y, 0);

        Vector3 dir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        if(hooked)
        {
            arrow.transform.localEulerAngles = new Vector3(0, 0, 0);

            switch (direction)
            {
                case 0:
                    dir += new Vector3(-1 * strength, 0, 0);

                    if(Input.GetAxis("Horizontal") > 0)
                    {
                        strength -= Time.deltaTime / 2;
                    }

                    arrow.transform.Rotate(Vector3.up, 90f);
                    break;
                case 1:
                    dir += new Vector3(strength, 0, 0);

                    if (Input.GetAxis("Horizontal") < 0)
                    {
                        strength -= Time.deltaTime / 2;
                    }

                    arrow.transform.Rotate(Vector3.up, 270f);
                    break;
                case 2:
                    dir += new Vector3(0, 0, -1 * strength);

                    if (Input.GetAxis("Vertical") > 0)
                    {
                        strength -= Time.deltaTime / 2;
                    }

                    arrow.transform.Rotate(Vector3.up, 0f);
                    break;
                case 3:
                    dir += new Vector3(0, 0, strength);

                    if (Input.GetAxis("Vertical") < 0)
                    {
                        strength -= Time.deltaTime / 2;
                    }

                    arrow.transform.Rotate(Vector3.up, 180f);
                    break;
            }

            if(nextChange < Time.timeSinceLevelLoad)
            {
                nextChange = Time.timeSinceLevelLoad + Random.Range(0f, 2f);
                direction = Random.Range(0, 4);
            }

            if(strength <= 0)
            {
                bm.ShowCatch();

                if (InputManager.submitDown)
                {
                    bm.HideCatch();
                    bm.SetDriving();
                    bm.fishCaught += weight;
                    Destroy(gameObject);
                }
            }
        }

        transform.Translate(dir * Time.deltaTime);
	}

    public void HookFish(float str)                                     //Called when a fish enters the trigger zone
    {
        hooked = true;
        strength = str;
        weight = str;
        splash.SetActive(true);
        arrow.SetActive(true);
    }

    private void OnTriggerStay(Collider other)                         //Change direction when colliding with something
    {
        if (hooked)
        {
            Vector3 collDir = other.transform.position - transform.position;

            if (Mathf.Abs(collDir.x) > Mathf.Abs(collDir.z))
            {
                if(collDir.x > 0)
                {
                    direction = 0;
                }
                else
                {
                    direction = 1;
                }
            }
            else
            {
                if (collDir.y > 0)
                {
                    direction = 2;
                }
                else
                {
                    direction = 3;
                }
            }

            //direction = Random.Range(0, 4);
        }
    }

    public void SetBobber(BoatManager boatManager)
    {
        bm = boatManager;
    }
}
