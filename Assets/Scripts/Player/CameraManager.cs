using UnityEngine;
using System.Collections;

//Moves object to follow target at fixed distance, can also be rotated around it. Moves closer to target when inside colliders.
//Also sets bloom and fog intensity based on distance from the town.

public class CameraManager : MonoBehaviour
{
    private float distance;                                                         //Distance from target
    public float distanceMin, distanceMax;
    private Vector3 tempPos;

    private bool colliding = false;
    public float cameraAngle = 45f;                                                 //Camera angle

    public float xSpeed = 80.0f;                                                    //Rotation speed
    private float xRot = 0.0f;                                                      //Current rotation

    public Transform target;                                                        //Whatever the camera is following
    public RectTransform compass;

    void Start()
    {
        Vector3 angles = transform.eulerAngles;                                     //Get initial xRot value
        xRot = angles.y;
        distance = distanceMax;                                                     //Set distance from target to maximum
        Cursor.lockState = CursorLockMode.Confined;                                 //Prevent cursor from leaving game window

    }

    void LateUpdate()
    {
        if (!GameManager.paused)                                                    //If game isn't paused
        {
            xRot += InputManager.cameraX * xSpeed * Time.deltaTime;                 //Add input value to xRot

            Cursor.visible = false;                                                 //Hide mouse cursor
        }
        else
        {
            Cursor.visible = true;                                                  //Show mouse cursor
        }

        if (colliding && distance > distanceMin)                                    //If inside a collider, move towards target
        {
            distance -= Time.deltaTime * 5f;
        }
        else if (!colliding && distance < distanceMax && Vector3.Distance(transform.position, tempPos) > 0.2f)  //Otherwise, if camera has moved away from last position sufficiently, move away from target, up to maximum distance
        {
            distance += Time.deltaTime * 5f;
        }

        Quaternion rotation = Quaternion.Euler(cameraAngle, xRot, 0);               //Create rotation data
        Vector3 position = new Vector3(0.0f, 0.0f, -distance);                      //Create position relative to origin
        position = rotation * position;                                             //Multiply position by rotation to rotate around origin
        position += target.position;                                                //Add target position to position to move to correct position

        transform.rotation = rotation;                                              //Set camera rotation
                                                                                    //target.rotation = Quaternion.Euler(0, xRot, 0);                             //Make target face away from camera
        transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * 6f);                   //Set camera position

        transform.LookAt(target.transform);                                         //Make camera look directly at target

        compass.transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.y); //Set compass needle rotation

        float camDistance = 1f - Vector3.Distance(transform.position, Vector3.zero) / 350f; //Represent distance from town as a value between 0 & 0.8
        camDistance = Mathf.Clamp(camDistance, 0, 0.8f);
    }

    private void OnTriggerStay(Collider other)                                      //If camera enters untagged collider, set colliding to true (OnTriggerStay might work better, but be less efficient)
    {
        if(other.tag == "Untagged")
        {
            colliding = true;
        }

    }

    private void OnTriggerExit(Collider other)                                      //If camera leaves untagged collider, set colliding to false
    {
        if (other.tag == "Untagged")
        {
            colliding = false;
            tempPos = transform.position;                                           //Record camera position, to make sure camera isn't stuck moving in & out while it is still
        }
    }

}