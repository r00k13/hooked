﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boat : MonoBehaviour
{
    public Transform motor;                                                     //Motor at back of boat
    private Rigidbody rb;
    private BoatManager bm;
    private ParticleSystem foam;
    
    public float speed = 300f;
    public float turnSpeed = 20f;

    private bool delay;

	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        bm = GetComponent<BoatManager>();
        foam = motor.GetComponentInChildren<ParticleSystem>();
	}

    private void OnEnable()
    {
        delay = true;
    }

    void FixedUpdate ()
    {
        float motorRot = motor.localEulerAngles.y;                              //Get current rotation of motor

        if(motorRot > 180f)                                                     //Set rotation to a value between -180 & 180
        {
            motorRot -= 360f;
        }

        if (Input.GetAxis("Horizontal") < -0.1f)                                //Rotate motor
        {
            if (motorRot < 45f)                                                 
            {
                motor.Rotate(new Vector3(0, -turnSpeed * Input.GetAxis("Horizontal") * Time.deltaTime, 0));
            }
        }
        else if (Input.GetAxis("Horizontal") > 0.1f)
        {
            if(motorRot > -45f)
            {
                motor.Rotate(new Vector3(0, -turnSpeed * Input.GetAxis("Horizontal") * Time.deltaTime, 0));
            }
        }
        else
        {
            motor.rotation = Quaternion.Lerp(motor.rotation, transform.rotation, Time.deltaTime * 3);   //Lerp motor back to neutral rotation
        }

        //if (Input.GetAxis("Vertical") > 0.1f || Input.GetAxis("Vertical") < -0.1f)
        //{
        //    rb.AddForceAtPosition(Input.GetAxis("Vertical") * motor.forward * speed * Time.deltaTime, motor.position);      //Add force from motor
        //    rb.AddForce(Input.GetAxis("Vertical") * transform.forward * speed * 0.2f * Time.deltaTime);                     //Add additional forward momentum

        //    if (motorRot > 5f)
        //    {
        //        rb.AddTorque(Vector3.up * 10 * -turnSpeed * Time.deltaTime);
        //    }
        //    else if (motorRot < -5f)
        //    {
        //        rb.AddTorque(Vector3.up * 10 * turnSpeed * Time.deltaTime);
        //    }
        //}
        //else 

        float inputSpeed = 0f;

        if (InputManager.triggerL > 0.1f || InputManager.triggerR > 0.1f)
        {
            inputSpeed = InputManager.triggerR - InputManager.triggerL;
            rb.AddForceAtPosition(inputSpeed * motor.forward * speed * Time.deltaTime, motor.position);      //Add force from motor
            //rb.AddForce(inputSpeed * transform.forward * speed * 0.2f * Time.deltaTime);                     //Add additional forward momentum  

            if (motorRot > 5f)
            {
                rb.AddTorque(Vector3.up * 10 * -turnSpeed * Time.deltaTime);
            }
            else if (motorRot < -5f)
            {
                rb.AddTorque(Vector3.up * 10 * turnSpeed * Time.deltaTime);
            }
        }

        ParticleSystem.EmissionModule emission = foam.emission;
        if (inputSpeed > 0.05f)
        {
            emission.rateOverTime = Mathf.Clamp(rb.velocity.magnitude * 2f, 0, 40);
        }
        else
        {
            emission.rateOverTime = 0;
        }

        float zRot = transform.eulerAngles.z;                                   //Check z-axis to see if boat is going to capsize
        if (zRot > 180f)
        {
            zRot -= 360f;
        }

        if (zRot > 10f)                                                         //If it is, add torque in opposite direction
        {
            Debug.Log("Torque -");
            rb.AddTorque(transform.forward * -3f * zRot * Time.deltaTime, ForceMode.Impulse);   
        }
        else if (zRot < -10f)
        {
            Debug.Log("Torque +");
            rb.AddTorque(transform.forward * -3f * zRot * Time.deltaTime, ForceMode.Impulse);
        }

        if(InputManager.submitDown && !delay)                                                 //If player presses submit button, switch to fishing mode
        {
            bm.SetFishing();
        }

        delay = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "WaterCollider")
        {
            GetComponent<Rigidbody>().AddForce((transform.position - collision.contacts[0].point) * 20, ForceMode.Impulse);
        }
    }
}
