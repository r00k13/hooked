﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{
    public float speed = 2f;                                                               //How fast the bird moves

    void Update()
    {
        if (!GameManager.paused)
        {
            float height = -0.3f - Mathf.PingPong(Time.time * speed, 0.3f);

            transform.position = new Vector3(0, height, 0);
        }
    }
}
