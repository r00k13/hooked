using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Toggles building roof on & off when the player enters or leaves a building

public class Building : MonoBehaviour
{
    public GameObject roof;

    void OnTriggerEnter(Collider collision)                     //When player enters trigger zone, disable roof
    {
        if (collision.gameObject.tag == "Player")
        {
            roof.SetActive(false);
        }
    }
    
    void OnTriggerExit(Collider collision)                      //When player exits trigger zone, enable roof
    {
        if (collision.gameObject.tag == "Player")
        {
            roof.SetActive(true);
        }
    }
}
