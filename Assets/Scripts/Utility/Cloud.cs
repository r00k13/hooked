using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Moves the object it is attached to forwards. A static float and vector3 representing wind speed and direction are shared between all clouds, so they all move the same way.
//These are randomly changed whenever a cloud moves off the edge of the map.

public class Cloud : MonoBehaviour
{
    private static float windSpeed = 6f;
    private static Vector3 windDirection = new Vector3(0, 180, 0);                                          //Rotation of cloud     

	void Update ()
    {
        if(!GameManager.paused)
        {
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, windDirection, Time.deltaTime);     //Rotate to match direction
            transform.Translate(transform.forward * windSpeed * Time.deltaTime);

            if (Vector3.Distance(transform.position, Vector3.zero) > 750)                                   //When cloud moves off edge of map
            {
                windDirection.y = transform.eulerAngles.y + Random.Range(-10f, 10f);                        //Randomly change wind speed & direction
                windDirection.y = Mathf.Clamp(windDirection.y, 0f, 360f);
                windSpeed += Random.Range(-0.5f, 0.5f);
                windSpeed = Mathf.Clamp(windSpeed, 4f, 8f);
                
                transform.position = new Vector3(0, transform.position.y, 0);                               //Reset cloud position
                transform.Translate(transform.forward * -650f);
                transform.Translate(transform.right * Random.Range(-300f, 300f));
            }
        }
	}
}
