﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour
{
    private float strength;
    private Rigidbody rb;

	void Start ()
    {
        rb = GetComponent<Rigidbody>();

        strength = Random.Range(2f, 3.5f);

        transform.localScale = transform.localScale * (strength / 2);

        transform.Rotate(Vector3.up, Random.Range(0, 360));
	}

	void Update ()
    {
        rb.MovePosition(transform.position + transform.forward * Time.deltaTime);

        if(Vector3.Distance(transform.position, Vector3.zero) > 300)
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<FishSpawner>().fishCount--;
            Destroy(gameObject);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bobber")
        {
            Debug.Log("Caught");

            Bobber bobber = other.gameObject.GetComponent<Bobber>();
            bobber.HookFish(strength);

            GameObject.FindGameObjectWithTag("GameController").GetComponent<FishSpawner>().fishCount--;

            Destroy(gameObject);
        }
    }
}
