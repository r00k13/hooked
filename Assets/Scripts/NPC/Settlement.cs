﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settlement : MonoBehaviour
{
    public float fish = 0;                                          //How much fish has been deposited
    public int goal = 10;                                           //Todays target amount of fish

    public Image goalImage;                                         //Bar that shows progress towards goal

    void Start ()
    {
        goalImage.fillAmount = fish / goal;
    }

    public void AddFish(float fishCaught)
    {
        fish += fishCaught;
        Debug.Log(fishCaught + "kg of fish deposited");
        goalImage.fillAmount = fish / goal;
    }
}
