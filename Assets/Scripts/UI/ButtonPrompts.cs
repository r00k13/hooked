using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Shows or hides an image that prompts the user to press a button, & sets the sprite used by the image to match the input method the player is using

public class ButtonPrompts : MonoBehaviour
{
    public Sprite btnA, btnX, btnL;                                     //Sprites for each input method

    private void Start()
    {
        StartCoroutine("SetInteractLater");                             //Set image once at start
    }

    public void Show()                                                  //Enable the gameObject
    {
        gameObject.SetActive(true);
        SetInteract();
    }

    public void Hide()                                                  //Disable the gameObject
    {
        gameObject.SetActive(false);
    }

    IEnumerator SetInteractLater()                                      //Wait for 0.05 seconds, then set image. Necessary to prevent issue where image is set before ready at scene start
    {
        yield return new WaitForSeconds(0.05f);
        SetInteract();
    }

    void SetInteract()
    {
        Image img = GetComponent<Image>();                              //Retrieve the image component 

        if (InputManager.type == InputManager.InputType.PS4)            //Set sprite based on input type
        {
            img.sprite = btnX;
        }
        else if (InputManager.type == InputManager.InputType.XBOX)
        {
            img.sprite = btnA;
        }
        else
        {
            img.sprite = btnL;
        }

    }
}
