using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
//using Fungus;
using UnityEngine.SceneManagement;

//A set of functions used in the main menu

public class MainMenuManager : MonoBehaviour
{
    public GameObject optionsCanvas;
    //public Flowchart flowchart;

    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ClearSaveData()                                 //Replace all data in the save file (or creates a new empty save file)                  
    {
        SaveData saveData = new SaveData();                     //Create save data representing a new game

        string jsonText = JsonUtility.ToJson(saveData);         //Assign save data
        File.WriteAllText(Application.persistentDataPath + "/" + "save.json", jsonText);
    }

    public void ShowOptions()                                   //Show the options menu
    {
        optionsCanvas.SetActive(true);
    }

    public void HideOptions()                                   //Hide the options menu & show the main menu again
    {
        optionsCanvas.SetActive(false);
        //flowchart.ExecuteBlock("OpenMenu");
    }
}
